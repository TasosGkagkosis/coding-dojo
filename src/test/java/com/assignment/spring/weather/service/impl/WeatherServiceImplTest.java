package com.assignment.spring.weather.service.impl;

import com.assignment.spring.weather.exception.ExternalSourceException;
import com.assignment.spring.weather.external.client.WeatherClient;
import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import com.assignment.spring.weather.model.entity.WeatherEntity;
import com.assignment.spring.weather.repository.WeatherRepository;
import com.assignment.spring.weather.service.WeatherService;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class WeatherServiceImplTest {

    private WeatherRepository weatherRepositoryMock;
    private WeatherClient weatherClientMock;
    private WeatherService weatherService;

    @Before
    public void init() {
        weatherRepositoryMock = mock(WeatherRepository.class);
        weatherClientMock = mock(WeatherClient.class);
        weatherService = new WeatherServiceImpl(weatherRepositoryMock, weatherClientMock);
    }

    @Test
    public void fetchAndSaveWeather_withExpectedInput_shouldGiveExpectedOutput() {
        //given
        final var givenNWeatherRequestDto = new WeatherRequestDto("Amsterdam");
        final var givenWeatherResponseDto = new WeatherResponseDto("Amsterdam", "NL", 5.0);
        final var givenWeatherEntity = new WeatherEntity(givenWeatherResponseDto);
        final var givenRequestId = UUID.randomUUID();
        final var expectedWeatherResponseDto = new WeatherResponseDto("Amsterdam", "NL", 5.0);

        //when
        Mockito.when(weatherRepositoryMock.save(givenWeatherEntity)).thenReturn(givenWeatherEntity);
        Mockito.when(weatherClientMock.fetchWeather(givenNWeatherRequestDto, givenRequestId)).thenReturn(givenWeatherResponseDto);
        var weatherResponseDto = weatherService.fetchAndSaveWeather(givenNWeatherRequestDto, givenRequestId);

        //then
        verify(weatherRepositoryMock, times(1)).save(givenWeatherEntity);
        verify(weatherClientMock, times(1)).fetchWeather(givenNWeatherRequestDto, givenRequestId);
        assertEquals(expectedWeatherResponseDto, weatherResponseDto);
    }

    @Test(expected = ExternalSourceException.class)
    public void fetchAndSaveWeather_withInvalidInput_shouldGiveExternalSourceException() {
        //given
        final var givenNWeatherRequestDto = new WeatherRequestDto("Amsterdam");
        final var givenRequestId = UUID.randomUUID();

        //when
        Mockito.when(weatherClientMock.fetchWeather(givenNWeatherRequestDto, givenRequestId)).thenThrow(new ExternalSourceException("message"));
        weatherService.fetchAndSaveWeather(givenNWeatherRequestDto, givenRequestId);

        //then
        verify(weatherClientMock, times(1)).fetchWeather(givenNWeatherRequestDto, givenRequestId);
        verify(weatherRepositoryMock, times(0)).save(any(WeatherEntity.class));
    }

    @Test(expected = DataAccessException.class)
    public void fetchAndSaveWeather_withDbRelatedIssue_shouldDataAccessException() {
        //given
        final var givenNWeatherRequestDto = new WeatherRequestDto("Amsterdam");
        final var givenWeatherResponseDto = new WeatherResponseDto("Amsterdam", "NL", 5.0);
        final var givenRequestId = UUID.randomUUID();

        //when
        Mockito.when(weatherClientMock.fetchWeather(givenNWeatherRequestDto, givenRequestId)).thenReturn(givenWeatherResponseDto);
        Mockito.when(weatherRepositoryMock.save(any(WeatherEntity.class))).thenThrow(new DataAccessException(""){});
        weatherService.fetchAndSaveWeather(givenNWeatherRequestDto, givenRequestId);

        //then
        verify(weatherClientMock, times(0)).fetchWeather(givenNWeatherRequestDto, givenRequestId);
        verify(weatherRepositoryMock, times(1)).save(any(WeatherEntity.class));
    }
}