package com.assignment.spring.weather.web.exception;

import com.assignment.spring.weather.exception.ExternalSourceException;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Log4j2
@RestControllerAdvice
public class ControllerApiExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleExternalSourceException(final ExternalSourceException ex) {
        log.error("Exception on: {}, message: {}, exception: {}", ex.getRequestId().get(), ex.getMessage(), ex);
        return createResponse(HttpStatus.BAD_GATEWAY, ex.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<ErrorResponse> handleDataAccessException(final DataAccessException ex) {
        log.error("Exception message: {}, exception: {}", ex.getMessage(), ex);
        return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    protected ResponseEntity<ErrorResponse> createResponse(HttpStatus status, String reason) {
        final ErrorResponse error = new ErrorResponse(status.value(), reason);
        return new ResponseEntity<>(error, status);
    }
}