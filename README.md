Spring Boot Coding Dojo
---

Welcome to the Spring Boot Coding Dojo!

### Introduction
Hello guys,

thanks for the opportunity to participate in this assignment.

LeasePlan is one of my top options because your need for a long term cooperation
feels like you want to create a tech family with good team spirit and bonding witch fits 
perfectly with my principles.

Please feel free to contact me in any case for a technical or none discussion or
further clarifications at **tasosgagosis@hotmail.com**.

### Database
As we are talking about a realistic production case the database of this service should not
be a simple dockerized instance but a more stable approach with persistence, backups etc.

Additionally, a docker compose of this service and the database again is not a production solution
but a combination to run it locally with once.

For these reasons my setup uses an embedded H2 memory based instance as I believe a proper database setup
is out of scope for this assignment.

This setup (H2 in memory) would be handy for e2e tests in this service.

### How to run
This project consists of a typical spring boot setup for the support of different environments
implemented by separated .properties files for each of them.

Activate each profile by switching the bellow property in .../resources/application.properties
```
spring.profiles.active=dev
```

For the **prod** version we need to hide our sensitive information as credentials, api-keys etc.
We will inject them to our app trough environmental variables:
```
export PROD_DB_PASS=dbPassword
export PROD_DB_USER=dbUser
export OPEN_WEATHER_API_KEY_PROD=key
```

Local run
```
mvn spring-boot:run
```

Dockerized version

build docker
```
mvn clean install

docker build -t coding/dojo .
```

run dev version:
```
docker run -d -p 8080:8080 coding/dojo
```

run prod version:
```
docker run -d -p 8080:8080 \
--env PROD_DB_USER=${PROD_DB_USER} \
--env PROD_DB_PASS=${PROD_DB_PASS} \
--env OPEN_WEATHER_API_KEY_PROD=${OPEN_WEATHER_API_KEY_PROD} \
coding/dojo 
```

### Testing
This service contains both unit tests and e2e testing.
Unit tests will be automatically executed during application build.
End to end tests (ApplicationE2e.java) will not be executed automatically as they use a full
Spring runtime context and the H2 db and they are not lightweight as unit tests.
Feel free to run them explicitly. 

### Future improvements /  food for thought
 - use a circuit breaker implementation in our rest clients to improve resiliency
 - use a more reactive approach in our controllers and database drivers like webflux to improve performance
 - use async logging
