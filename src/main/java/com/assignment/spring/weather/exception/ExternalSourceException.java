package com.assignment.spring.weather.exception;

import java.util.UUID;

public class ExternalSourceException extends AbstractException {
    public ExternalSourceException(String message, UUID requestId) {
        super(message, requestId);
    }

    public ExternalSourceException(String message) {
        super(message, null);
    }
}
