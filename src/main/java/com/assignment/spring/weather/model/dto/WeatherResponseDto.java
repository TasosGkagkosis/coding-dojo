package com.assignment.spring.weather.model.dto;

import com.assignment.spring.weather.external.dto.OpenWeatherResponseDto;
import com.assignment.spring.weather.model.entity.WeatherEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class WeatherResponseDto {
    private String city;
    private String country;
    private Double temperature;

    public WeatherResponseDto(WeatherEntity weatherEntity){
        this.setCity(weatherEntity.getCity());
        this.setCountry(weatherEntity.getCountry());
        this.setTemperature(weatherEntity.getTemperature());
    };

    public WeatherResponseDto(OpenWeatherResponseDto openWeatherResponseDto){
        this.setCity(openWeatherResponseDto.getName());
        this.setCountry(openWeatherResponseDto.getSys().getCountry());
        this.setTemperature(openWeatherResponseDto.getMain().getTemp());
    };
}
