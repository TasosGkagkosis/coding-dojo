package com.assignment.spring.weather.repository;

import com.assignment.spring.weather.model.entity.WeatherEntity;
import org.springframework.data.repository.CrudRepository;

public interface WeatherRepository extends CrudRepository<WeatherEntity, Integer> {
}
