package com.assignment.spring.weather;

import com.assignment.spring.weather.external.dto.Main;
import com.assignment.spring.weather.external.dto.OpenWeatherResponseDto;
import com.assignment.spring.weather.external.dto.Sys;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import com.assignment.spring.weather.web.exception.ErrorResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

import static com.assignment.spring.weather.util.Constants.EXTERNAL_SOURCE_EXCEPTION_MESSAGE;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@TestPropertySource("classpath:application-test.properties")
public class ApplicationE2e {

    @Autowired
    private RestTemplate mockRestTemplate;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Value("${open.weather.map.url}")
    private String openWeatherUrl;

    private MockRestServiceServer mockServer;

    @Before
    public void init(){
        mockServer = MockRestServiceServer.createServer(this.mockRestTemplate);
    }

    @Test
    public void e2e_withExpectedInput_shouldGiveExpectedOutput() throws Exception {
        //given
        final var givenMain = new Main();
        givenMain.setTemp(5.0);

        final var givenSys = new Sys();
        givenSys.setCountry("NL");

        final var givenOpenWeatherResponseDto = new OpenWeatherResponseDto();
        givenOpenWeatherResponseDto.setName("Amsterdam");
        givenOpenWeatherResponseDto.setMain(givenMain);
        givenOpenWeatherResponseDto.setSys(givenSys);

        final var expectedWeatherResponse = "{\"coord\":{\"lon\":4.8897,\"lat\":52.374},\"weather\":[{\"id\":741,\"main\":\"Fog\",\"description\":\"fog\",\"icon\":\"50n\"},{\"id\":701,\"main\":\"Mist\",\"description\":\"mist\",\"icon\":\"50n\"}],\"base\":\"stations\",\"main\":{\"temp\":277.85,\"feels_like\":277.85,\"temp_min\":276.94,\"temp_max\":278.66,\"pressure\":1033,\"humidity\":98},\"visibility\":1300,\"wind\":{\"speed\":0.45,\"deg\":199,\"gust\":2.24},\"clouds\":{\"all\":100},\"dt\":1642530762,\"sys\":{\"type\":2,\"id\":2005807,\"country\":\"NL\",\"sunrise\":1642491625,\"sunset\":1642521646},\"timezone\":3600,\"id\":2759794,\"name\":\"Amsterdam\",\"cod\":200}";
        final var expectedWeatherResponseDto = new WeatherResponseDto("Amsterdam", "NL", 277.85);

        //when
        mockServer.expect(ExpectedCount.once(), requestTo(new URI(openWeatherUrl.replace("[city]", givenOpenWeatherResponseDto.getName()))))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(expectedWeatherResponse)
                );

        ResponseEntity<WeatherResponseDto> weatherResponse = testRestTemplate.getForEntity("/weather/Amsterdam", WeatherResponseDto.class);

        //then
        assertEquals(HttpStatus.OK, weatherResponse.getStatusCode());
        assertEquals(expectedWeatherResponseDto, weatherResponse.getBody());
    }

    @Test
    public void e2e_withInvalidInput_shouldGive502BadGateway() throws Exception {
        //given
        final var givenInvalidCity = "invalid-city";
        final ErrorResponse expectedErrorResponse = new ErrorResponse(HttpStatus.BAD_GATEWAY.value(), EXTERNAL_SOURCE_EXCEPTION_MESSAGE);

        //when
        mockServer.expect(ExpectedCount.once(), requestTo(new URI(openWeatherUrl.replace("[city]", givenInvalidCity))))
                .andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.NOT_FOUND)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body("")
                );

        ResponseEntity<ErrorResponse> weatherResponse = testRestTemplate.getForEntity("/weather/invalid-city", ErrorResponse.class);

        //then
        assertEquals(HttpStatus.BAD_GATEWAY, weatherResponse.getStatusCode());
        assertEquals(expectedErrorResponse.getStatus(), weatherResponse.getBody().getStatus());
        assertEquals(expectedErrorResponse.getMessage(), weatherResponse.getBody().getMessage());
    }
}