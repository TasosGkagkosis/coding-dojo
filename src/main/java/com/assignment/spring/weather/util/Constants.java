package com.assignment.spring.weather.util;

public class Constants {
    public static final String EXTERNAL_SOURCE_EXCEPTION_MESSAGE = "An error occurred connecting an external source.";
}
