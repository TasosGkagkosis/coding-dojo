package com.assignment.spring.weather.service;


import com.assignment.spring.weather.exception.ExternalSourceException;
import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import com.assignment.spring.weather.model.entity.WeatherEntity;
import org.springframework.dao.DataAccessException;

import java.util.UUID;

public interface WeatherService {
    WeatherResponseDto fetchAndSaveWeather(WeatherRequestDto weatherRequestDto, UUID requestId) throws DataAccessException, ExternalSourceException;
    WeatherEntity saveWeather(WeatherEntity weatherEntity, UUID requestId) throws DataAccessException;
}
