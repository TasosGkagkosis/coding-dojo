package com.assignment.spring.weather.web.exception;

import lombok.Data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class ErrorResponse {

    private final int status;
    private final String message;
    private String timestamp = ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
}