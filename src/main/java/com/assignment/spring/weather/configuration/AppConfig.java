package com.assignment.spring.weather.configuration;

import com.assignment.spring.weather.external.client.WeatherClient;
import com.assignment.spring.weather.external.client.impl.OpenWeatherClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {

    @Value("${open.weather.map.url}")
    private String openWeatherUrl;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public String getOpenWeatherUrl(){return this.openWeatherUrl;}

    @Bean
    public WeatherClient weatherClient(RestTemplate restTemplate, String openWeatherUrl) {
         return new OpenWeatherClient(restTemplate, openWeatherUrl);
    }
}
