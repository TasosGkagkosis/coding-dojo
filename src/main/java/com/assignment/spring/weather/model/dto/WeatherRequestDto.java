package com.assignment.spring.weather.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class WeatherRequestDto {
    private String city;
}
