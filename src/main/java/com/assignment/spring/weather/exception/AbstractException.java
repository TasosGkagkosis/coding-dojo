package com.assignment.spring.weather.exception;

import lombok.Data;

import java.util.Optional;
import java.util.UUID;

@Data
public class AbstractException extends RuntimeException {

    private final Optional<UUID> requestId;
    
    public AbstractException(String message, UUID requestId) {
        super(message);
        this.requestId = Optional.ofNullable(requestId);
    }
}
