package com.assignment.spring.weather.external.client;

import com.assignment.spring.weather.exception.ExternalSourceException;
import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;

import java.util.UUID;

public interface WeatherClient {
    WeatherResponseDto fetchWeather(WeatherRequestDto weatherRequestDto, UUID requestId) throws ExternalSourceException;
}