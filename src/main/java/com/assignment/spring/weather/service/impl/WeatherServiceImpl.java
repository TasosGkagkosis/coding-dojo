package com.assignment.spring.weather.service.impl;

import com.assignment.spring.weather.exception.ExternalSourceException;
import com.assignment.spring.weather.external.client.WeatherClient;
import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import com.assignment.spring.weather.model.entity.WeatherEntity;
import com.assignment.spring.weather.repository.WeatherRepository;
import com.assignment.spring.weather.service.WeatherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;


import java.util.UUID;

@Log4j2
@RequiredArgsConstructor
@Service
public class WeatherServiceImpl implements WeatherService {

    private final WeatherRepository weatherRepository;
    private final WeatherClient weatherClient;

    @Override
    public WeatherResponseDto fetchAndSaveWeather(WeatherRequestDto weatherRequestDto, UUID requestId) throws DataAccessException, ExternalSourceException {
        final var METHOD = "fetchAndSaveWeather";
        log.info("method:{}, requestId:{}, input:{}", METHOD, requestId, weatherRequestDto);

        var weatherResponseDto = weatherClient.fetchWeather(weatherRequestDto, requestId);
        var weatherEntity = saveWeather(new WeatherEntity(weatherResponseDto), requestId);
        var response = new WeatherResponseDto(weatherEntity);

        log.info("method:{}, requestId:{}, output:{}, success", METHOD, requestId, response);
        return response;
    }

    @Override
    public WeatherEntity saveWeather(WeatherEntity weatherEntity, UUID requestId) throws DataAccessException {
        final var METHOD = "saveWeather";
        log.info("method:{}, requestId:{}, input:{}", METHOD, requestId, weatherEntity);

        var response = weatherRepository.save(weatherEntity);

        log.info("method:{}, requestId:{}, output:{}, success", METHOD, requestId, response);
        return response;
    }
}
