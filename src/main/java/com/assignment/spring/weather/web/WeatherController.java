package com.assignment.spring.weather.web;

import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import com.assignment.spring.weather.service.WeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RequiredArgsConstructor
@RestController
@RequestMapping({ "/weather" })
public class WeatherController {

    private final WeatherService weatherService;

    @GetMapping(path = "/{city}")
    public ResponseEntity<WeatherResponseDto> weather(@PathVariable("city") String city) throws Exception {
        var response = weatherService.fetchAndSaveWeather(new WeatherRequestDto(city), UUID.randomUUID());
        return new ResponseEntity(response, HttpStatus.OK);
    }
}
