package com.assignment.spring.weather.external.client.impl;

import com.assignment.spring.weather.exception.ExternalSourceException;
import com.assignment.spring.weather.external.client.WeatherClient;
import com.assignment.spring.weather.external.dto.Main;
import com.assignment.spring.weather.external.dto.OpenWeatherResponseDto;
import com.assignment.spring.weather.external.dto.Sys;
import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class OpenWeatherClientTest{

    private RestTemplate restTemplateMock;
    private WeatherClient weatherClient;
    private String openWeatherUrlMock;

    @Before
    public void init() {
        openWeatherUrlMock = "http://api.openweathermap.org/data/2.5/weather?q=Amsterdam&APPID=xxx";
        restTemplateMock = mock(RestTemplate.class);
        weatherClient = new OpenWeatherClient(restTemplateMock, openWeatherUrlMock);
    }

    @Test
    public void fetchWeather_withExpectedOpenWeatherResponse_shouldGiveExpectedOutput() {
        //given
        final var givenNWeatherRequestDto = new WeatherRequestDto("Amsterdam");
        final var givenRequestId = UUID.randomUUID();

        final var givenMain = new Main();
        givenMain.setTemp(5.0);

        final var givenSys = new Sys();
        givenSys.setCountry("NL");

        final var givenOpenWeatherResponseDto = new OpenWeatherResponseDto();
        givenOpenWeatherResponseDto.setName("Amsterdam");
        givenOpenWeatherResponseDto.setMain(givenMain);
        givenOpenWeatherResponseDto.setSys(givenSys);

        final var expectedWeatherResponseDto = new WeatherResponseDto("Amsterdam", "NL", 5.0);;

        //when
        Mockito.when(restTemplateMock.getForEntity(openWeatherUrlMock, OpenWeatherResponseDto.class))
                .thenReturn(new ResponseEntity(givenOpenWeatherResponseDto, HttpStatus.OK));
        var weatherResponseDto = weatherClient.fetchWeather(givenNWeatherRequestDto, givenRequestId);

        //then
        assertEquals(expectedWeatherResponseDto, weatherResponseDto);
    }


    @Test(expected = ExternalSourceException.class)
    public void fetchWeather_withErrorResponse_shouldGiveExternalSourceException() {
        //given
        final var givenNWeatherRequestDto = new WeatherRequestDto("Amsterdam");
        final var givenRequestId = UUID.randomUUID();

        //when
        Mockito.when(restTemplateMock.getForEntity(openWeatherUrlMock, OpenWeatherResponseDto.class))
                .thenReturn(new ResponseEntity(new OpenWeatherResponseDto(), HttpStatus.NOT_FOUND));
        weatherClient.fetchWeather(givenNWeatherRequestDto, givenRequestId);

        //then
        verify(restTemplateMock, times(1)).getForEntity(anyString(), OpenWeatherResponseDto.class);
    }

    @Test(expected = ExternalSourceException.class)
    public void fetchWeather_withRestClientException_shouldGiveExternalSourceException() {
        //given
        final var givenNWeatherRequestDto = new WeatherRequestDto("Amsterdam");
        final var givenRequestId = UUID.randomUUID();

        //when
        Mockito.when(restTemplateMock.getForEntity(openWeatherUrlMock, OpenWeatherResponseDto.class))
                .thenThrow(new RestClientException(""));
        weatherClient.fetchWeather(givenNWeatherRequestDto, givenRequestId);

        //then
        verify(restTemplateMock, times(1)).getForEntity(anyString(), OpenWeatherResponseDto.class);
    }
}