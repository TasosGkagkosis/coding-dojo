package com.assignment.spring.weather.external.client.impl;

import com.assignment.spring.weather.exception.ExternalSourceException;
import com.assignment.spring.weather.external.client.WeatherClient;
import com.assignment.spring.weather.external.dto.OpenWeatherResponseDto;
import com.assignment.spring.weather.model.dto.WeatherRequestDto;
import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

import static com.assignment.spring.weather.util.Constants.EXTERNAL_SOURCE_EXCEPTION_MESSAGE;

@Log4j2
@RequiredArgsConstructor
@Component
public class OpenWeatherClient implements WeatherClient {

    private final RestTemplate restTemplate;
    private final String openWeatherUrl;

    @Override
    public WeatherResponseDto fetchWeather(WeatherRequestDto weatherRequestDto, UUID requestId) throws ExternalSourceException {
        final var METHOD = "fetchWeather";
        log.info("method:{}, requestId:{}, input:{}", METHOD,requestId, weatherRequestDto);

        try {
            var url = openWeatherUrl.replace("[city]", weatherRequestDto.getCity());
            var openWeatherResponse = restTemplate.getForEntity(url, OpenWeatherResponseDto.class);

            if (!openWeatherResponse.getStatusCode().is2xxSuccessful()){
                log.error("External source call was not 2XX OK, response:{}", openWeatherResponse);
                throw new ExternalSourceException(EXTERNAL_SOURCE_EXCEPTION_MESSAGE, requestId);
            }
            var response = new WeatherResponseDto(openWeatherResponse.getBody());

            log.info("method:{}, requestId:{}, output:{}, success", METHOD, requestId, response);
            return response;
        } catch (RestClientException e) {
            log.error("RestClientException exception occurred.", e);
            throw new ExternalSourceException(EXTERNAL_SOURCE_EXCEPTION_MESSAGE, requestId);
        }
    }
//    here I choose to log and hide the details of the technical problem (status code, description etc)
//    occurred with our third party from the end user giving back a '502 BAD GATEWAY'
//    depending on our business we can expose more details
}