package com.assignment.spring.weather.model.entity;

import com.assignment.spring.weather.model.dto.WeatherResponseDto;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@Entity
@Table(name = "weather")
public class WeatherEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String city;
    private String country;
    private Double temperature;

    public WeatherEntity(WeatherResponseDto weatherResponseDto){
        this.setCity(weatherResponseDto.getCity());
        this.setCountry(weatherResponseDto.getCountry());
        this.setTemperature(weatherResponseDto.getTemperature());
    }

}
